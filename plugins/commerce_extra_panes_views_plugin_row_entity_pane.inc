<?php

/**
 * @file
 * Row style plugin for displaying the results as entities.
 */

/**
 * Plugin class for displaying Views results with entity_view.
 */
class commerce_extra_panes_views_plugin_row_entity_pane extends views_plugin_row {

  protected $entity_type, $entities, $extra_entity;

  public function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    // Initialize the entity-type used.
    $table_data = views_fetch_data($this->view->base_table);
    $this->entity_type = $table_data['table']['entity type'];
    // Set base table and field information as used by views_plugin_row to
    // select the entity id if used with default query class.
    $info = entity_get_info($this->entity_type);
    if (!empty($info['base table']) && $info['base table'] == $this->view->base_table) {
      $this->base_table = $info['base table'];
      $this->base_field = $info['entity keys']['id'];
    }
  }

  public function option_definition() {
    $options = parent::option_definition();
    $options['extra_id'] = array('default' => '');
    $options['extra_type'] = array('default' => 'node');
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $entity_type = $this->entity_type;

    if ($entity_type != 'commerce_order') {
      // This module designed to work only with 'Commerce order' entities.
      return;
    }

    // $extra_type
    $extra_type = $this->options['extra_type'];
    if (empty($extra_type)) {
      $extra_type = 'node';
    }
    $entities_info = entity_get_info();

    $extra_type_options = array();
    if (!empty($entities_info)) {
      foreach ($entities_info as $type => $info) {
        $extra_type_options[$type] = $info['label'];
      }
    }

    // TODO: Use ajax to load 1) $extra_info and 2) $extra_panes after we changed $extra_type.
    $extra_info = entity_get_info($extra_type);

    $label_property  = $extra_info['entity keys']['label'];
    $bundle_property = $extra_info['entity keys']['bundle'];

    // $extra_id
    $extra_id = $this->options['extra_id'];
    if (empty($extra_id)) {
      $extra_id = NULL;
    }

    $extra_panes = commerce_extra_panes_get_panes($extra_id, $extra_type);

    $extra_id_options = array();
    if (!empty($extra_panes)) {
      $extra_entities = entity_load($extra_type, array_keys($extra_panes));

      foreach ($extra_panes as $id => $settings) {
        if ($settings->status && $extra_entities[$id]->status) {
          $label = $extra_entities[$id]->$label_property;
          $bundle = $extra_entities[$id]->$bundle_property;
          $helper_key = $extra_type . ':' . $bundle . ':' . $id;

          $extra_id_options[$id] = $label . ' (' . $helper_key . ')';
        }
      }
    }

    // extra_type_options
    $extra_type_options_count = count($extra_type_options);
    if ($extra_type_options_count > 1) {
      $form['extra_type'] = array(
        '#type' => 'select',
        '#options' => $extra_type_options,
        '#title' => t('Extra type'),
        '#default_value' => $this->options['extra_type'],
      );
    }
    else if ($extra_type_options_count == 1) {
      $form['extra_type_info'] = array(
        '#type' => 'item',
        '#title' => t('Extra type'),
        '#description' => t('Only one Extra type is available.'),
        '#markup' => /*$extra_type_options ?*/ current($extra_type_options) /*: t('Default')*/,
      );

      $form['extra_type'] = array(
        '#type' => 'value',
        '#value' => /*$extra_type_options ?*/ key($extra_type_options) /*: 'default'*/,
      );
    }
    else {
      $form['extra_type_info'] = array(
        '#type' => 'item',
        '#title' => t('Extra type'),
        '#description' => t('No one Extra type is available. Try to use default one.'),
        '#markup' =>  t('Default'),
      );

      $form['extra_type'] = array(
        '#type' => 'value',
        '#value' => 'default',
      );
    }

    // $extra_id_options
    $extra_id_options_count = count($extra_id_options);
    if ($extra_id_options_count > 1) {
      $form['extra_id'] = array(
        '#type' => 'select',
        '#options' => $extra_id_options,
        '#title' => t('Extra ID'),
        '#default_value' => $this->options['extra_id'],
      );
    }
    else if ($extra_id_options_count == 1) {
      $form['extra_id_info'] = array(
        '#type' => 'item',
        '#title' => t('Extra ID'),
        '#description' => t('Only one Extra ID is available for this entity type.'),
        '#markup' => /*$extra_id_options ?*/ current($extra_id_options) /*: t('Default')*/,
      );
      $form['extra_id'] = array(
        '#type' => 'value',
        '#value' => /*$extra_id_options ?*/ key($extra_id_options) /*: 'default'*/,
      );
    }
    else {
      $form['extra_id_info'] = array(
        '#type' => 'item',
        '#title' => t('Extra ID'),
        '#description' => t('No one Extra ID is available. Try to use default one.'),
        '#markup' =>  t('Default'),
      );

      $form['extra_id'] = array(
        '#type' => 'value',
        '#value' => 'default',
      );
    }

    return $form;
  }

  public function pre_render($values) {
    if (!empty($values)) {
      list($this->entity_type, $this->entities) = $this->view->query->get_result_entities($values, !empty($this->relationship) ? $this->relationship : NULL, isset($this->field_alias) ? $this->field_alias : NULL);
    }
    // Render the entities.
    if ($this->entities && !empty($this->options['extra_id'])) {
      $extra_type = $this->options['extra_type'];
      $extra_id = $this->options['extra_id'];

      $this->extra_entity = entity_load_single($extra_type, $extra_id);
      if ($this->extra_entity->status) {
        // Replace tokens in the rendered content.
        $this->extra_entity->rendered = entity_view($extra_type, array($this->extra_entity), 'checkout_pane');
        $this->extra_entity->rendered['#markup'] = theme('commerce_extra_panes_review', array('entity' => $this->extra_entity));
      }
    }
  }

  /**
   * Overridden to return the entity object.
   */
  function get_value($values, $field = NULL) {
    return isset($this->entities[$this->view->row_index]) ? $this->entities[$this->view->row_index] : FALSE;
  }

  public function render($values) {
    if ($entity = $this->get_value($values)) {
      return token_replace($this->extra_entity->rendered['#markup'], array($this->options['extra_type'] => $this->extra_entity, 'commerce-order' => $entity));
    }
  }
}
