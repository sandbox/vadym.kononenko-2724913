<?php

/**
 * @file
 * Views row style to output nodes through commerce extra pane template.
 */

/**
 * Implements hook_views_plugins().
 */
function commerce_extra_panes_views_views_plugins() {
  // Have views cache the table list for us so it gets
  // cleared at the appropriate times.
  $data = views_cache_get('entity_base_tables', TRUE);
  if (!empty($data->data)) {
    $base_tables = $data->data;
  }
  else {
    $base_tables = array();
    foreach (views_fetch_data() as $table => $data) {
      if (!empty($data['table']['entity type']) && !empty($data['table']['base'])) {
        $base_tables[] = $table;
      }
    }
    views_cache_set('entity_base_tables', $base_tables, TRUE);
  }
  if (!empty($base_tables)) {
    return array(
      'module' => 'commerce_extra_panes_views',
      'row' => array(
        'commerce_extra_panes_views' => array(
          'title' => t('Commerce Extra Panes'),
          'help' => t('Renders order as order\'s \'commerce_extra_panes\'.'),
          'handler' => 'commerce_extra_panes_views_plugin_row_entity_pane',
          'uses fields' => FALSE,
          'uses options' => TRUE,
          'type' => 'normal',
          'base' => $base_tables,
        ),
      ),
    );
  }
}